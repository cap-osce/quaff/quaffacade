package com.capgemini.osce.quaff.quaffacade.data

data class KubernetesData(val tickets: List<Pod>) : QuafData {

    data class Pod(
        val id: String,
        val status: String
    )

}