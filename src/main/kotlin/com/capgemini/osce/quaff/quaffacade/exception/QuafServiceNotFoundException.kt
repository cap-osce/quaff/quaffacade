package com.capgemini.osce.quaff.quaffacade.exception

class QuafServiceNotFoundException(message: String?) : Exception(message) {

}
