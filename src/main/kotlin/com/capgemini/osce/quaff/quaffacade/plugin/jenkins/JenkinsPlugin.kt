package com.capgemini.osce.quaff.quaffacade.plugin.jenkins

import com.capgemini.osce.quaff.quaffacade.data.JenkinsData
import com.capgemini.osce.quaff.quaffacade.data.KubernetesData
import com.capgemini.osce.quaff.quaffacade.data.QuafData
import com.capgemini.osce.quaff.quaffacade.plugin.QuafPlugin
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate

class JenkinsPlugin(
        val baseUrl: String
) : QuafPlugin {

    val restTemplate = RestTemplate();

    override fun key(): String {
        return "jenkins"
    }

    override fun configure(properties: Map<String, String>) {

    }

    override fun update() : QuafData {

        val response = restTemplate.getForEntity(baseUrl + "/jenkins", List::class.java) as ResponseEntity<ArrayList<LinkedHashMap<String, String>>>

        var builds = ArrayList<JenkinsData.BuildInfo>()
        for (map in response.body!!) {
            builds.add(convert(map))
        }
        return JenkinsData(builds)
    }

    fun convert(map: LinkedHashMap<String, String>) : JenkinsData.BuildInfo  {
        return JenkinsData.BuildInfo(map.get("name")!!, map.get("url")!!, map.get("color")!!)
    }

}