package com.capgemini.osce.quaff.quaffacade.plugin

import com.capgemini.osce.quaff.quaffacade.plugin.jenkins.JenkinsPlugin
import com.capgemini.osce.quaff.quaffacade.plugin.jira.JiraPlugin
import com.capgemini.osce.quaff.quaffacade.plugin.k8.K8Plugin
import org.springframework.stereotype.Component

@Component
class PluginFactory(
        val configuration: Configuration
) {
    fun createFromName(name: String) : QuafPlugin =
        when  {
            name.startsWith("jenkins") -> JenkinsPlugin(configuration.jenkinsjuicerUrl)
            name.startsWith("jira") -> JiraPlugin(configuration.bugswillerUrl)
            name.startsWith("kubernetes") || name.startsWith("k8") -> K8Plugin(configuration.slurpernetesUrl)
            else -> throw Exception("No QuafPlugin defined for type ${name}")
        }

}