package com.capgemini.osce.quaff.quaffacade.plugin

import com.capgemini.osce.quaff.quaffacade.data.QuafData

interface QuafPlugin {

    /**
     * Unique Key for this plugin
     */
    fun key() : String

    /**
     * Get state from your client
     */
    fun configure(properties: Map<String, String>)

    /**
     * Update the state
     */
    fun update() : QuafData

}