package com.capgemini.osce.quaff.quaffacade.plugin.k8

import com.capgemini.osce.quaff.quaffacade.data.KubernetesData
import com.capgemini.osce.quaff.quaffacade.data.QuafData
import com.capgemini.osce.quaff.quaffacade.plugin.QuafPlugin
import org.springframework.http.ResponseEntity
import org.springframework.web.client.RestTemplate
import kotlin.collections.ArrayList

class K8Plugin(val baseUrl : String) : QuafPlugin {

    companion object {
        val namespace_property = "namespace"
    }

    val restTemplate = RestTemplate()

    var namespace = "DEFAULT";

    override fun key(): String {
        return "k8" + namespace;
    }

    override fun configure(properties: Map<String, String>) {
        this.namespace = properties.get(K8Plugin.namespace_property)!!
    }

    override fun update() : QuafData {
        return KubernetesData(callSlupernetes(namespace))
    }

    fun callSlupernetes(namespace: String) : ArrayList<KubernetesData.Pod> {
        val response = restTemplate.getForEntity(baseUrl + "/${namespace}", List::class.java) as ResponseEntity<ArrayList<LinkedHashMap<String, String>>>
        var pods = ArrayList<KubernetesData.Pod>()
        for (map in response.body!!) {
            pods.add(convert(map))
        }
        return pods
    }

    fun convert(map: LinkedHashMap<String, String>) : KubernetesData.Pod {
        return KubernetesData.Pod(map.get("id")!!, map.get("status")!!)
    }

}