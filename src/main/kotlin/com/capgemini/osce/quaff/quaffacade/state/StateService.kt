package com.capgemini.osce.quaff.quaffacade.state

import com.capgemini.osce.quaff.quaffacade.data.QuafData
import com.capgemini.osce.quaff.quaffacade.exception.QuafServiceNotFoundException
import com.capgemini.osce.quaff.quaffacade.plugin.PluginFactory
import com.capgemini.osce.quaff.quaffacade.plugin.QuafPlugin
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.util.logging.Logger

@Service
class StateService(private val pluginFactory: PluginFactory) {

    var plugins = ArrayList<QuafPlugin>()

    var logger = Logger.getLogger(this.javaClass.name)

    var state : MutableMap<String, QuafData> = mutableMapOf()

    fun update(key: String, data: QuafData) {
        state.put(key, data)
    }

    fun get(key: String, properties: Map<String, String>) : QuafData? {
        if (state.containsKey(key)) {
            return state.get(key)
        } else {
            val plugin = pluginFactory.createFromName(key)
            plugin.configure(properties)
            plugins.add(plugin);
            return plugin.update();
        }
    }

    //every minute
    @Scheduled(fixedDelay = 1000 * 5)
    fun trigger() =
        plugins.forEach {
            p -> update(p.key(), p.update())
            logger.info("Requesting new data from swillers and juicers...")
        }


}