package com.capgemini.osce.quaff.quaffacade.data

data class JiraData(val projects: List<Sprints>) : QuafData {

    data class Projects(
        val projects : List<Project>
    )

    data class Sprints(
        var projectId: String?,
        val sprints : List<Sprint>
    )

    data class Sprint(
        val id: String,
        val status: String
    )

    data class Project(
        val id: String,
        val name: String) {
    }

}