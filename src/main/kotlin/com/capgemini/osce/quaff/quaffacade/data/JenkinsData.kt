package com.capgemini.osce.quaff.quaffacade.data

data class JenkinsData(val builds: List<BuildInfo>) : QuafData {

    data class BuildInfo(
        val name: String,
        val url: String,
        val colour: String
    )

}