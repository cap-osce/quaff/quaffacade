package com.capgemini.osce.quaff.quaffacade.controller

import com.capgemini.osce.quaff.quaffacade.data.QuafData
import com.capgemini.osce.quaff.quaffacade.exception.QuafServiceNotFoundException
import com.capgemini.osce.quaff.quaffacade.plugin.k8.K8Plugin
import com.capgemini.osce.quaff.quaffacade.state.StateService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("quaff")
@CrossOrigin
class ChromsumerController(private val stateService: StateService) {

    @GetMapping("/{service}")
    fun get(@PathVariable service: String) : QuafData? {
        return stateService.get(service, emptyMap())
    }

    @GetMapping("/kubernetes/{namespace}")
    fun kubernetes(@PathVariable namespace: String) : QuafData? {
        return stateService.get("k8-" + namespace, mapOf(K8Plugin.namespace_property to namespace))
    }

    @ExceptionHandler(QuafServiceNotFoundException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun serviceNotFound(e: QuafServiceNotFoundException) = e.message

}