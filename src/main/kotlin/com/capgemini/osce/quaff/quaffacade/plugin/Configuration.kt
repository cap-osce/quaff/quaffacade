package com.capgemini.osce.quaff.quaffacade.plugin

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties
class Configuration(
        @Value("\${bugswiller.baseurl}") val bugswillerUrl: String,
        @Value("\${slurpernetes.baseurl}") val slurpernetesUrl: String,
        @Value("\${jenkinsjuicer.baseurl}") val jenkinsjuicerUrl: String
) {

}
