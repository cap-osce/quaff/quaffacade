package com.capgemini.osce.quaff.quaffacade.plugin.jira

import com.capgemini.osce.quaff.quaffacade.data.JiraData
import com.capgemini.osce.quaff.quaffacade.data.QuafData
import com.capgemini.osce.quaff.quaffacade.plugin.QuafPlugin
import org.springframework.web.client.RestTemplate

class JiraPlugin(val baseUrl : String) : QuafPlugin {

    val restTemplate = RestTemplate();


    override fun key(): String {
        return "jira"
    }

    override fun update(): QuafData {
        val projects = restTemplate.getForEntity(baseUrl + "/projects", JiraData.Projects::class.java)
        var projectSprints = ArrayList<JiraData.Sprints>()
        for (project in projects.body!!.projects) {
            var sprints = restTemplate.getForEntity(baseUrl + "/projects/${project.id}", JiraData.Sprints::class.java)
            sprints.body?.run {
                projectId = project.id
            }
            projectSprints.add(sprints.body!!)
        }
        return JiraData(projectSprints)
    }

    override fun configure(properties: Map<String, String>) {
        //
    }

}