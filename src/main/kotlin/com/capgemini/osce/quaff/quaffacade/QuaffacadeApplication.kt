package com.capgemini.osce.quaff.quaffacade


import com.capgemini.osce.quaff.quaffacade.state.StateService
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.scheduling.annotation.EnableScheduling

@SpringBootApplication
@EnableScheduling
class QuaffacadeApplication {

    //populate with some example data during dev
    @Bean
    fun init(stateService: StateService) = CommandLineRunner {
        stateService.trigger()
    }

}

fun main(args: Array<String>) {
    runApplication<QuaffacadeApplication>(*args)
}


